package db.test.app.product.filter;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FilterConfig {
    @Bean
    public FilterRegistrationBean<SimpleOncePerRequestFilter> logFilter() {
        FilterRegistrationBean<SimpleOncePerRequestFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(new SimpleOncePerRequestFilter());
        registrationBean.addUrlPatterns("/products/*");
        return registrationBean;
    }
}
